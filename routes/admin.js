const express = require('express');
const { body } = require('express-validator');

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// GET => /admin/add-product
router.get('/add-product', isAuth, adminController.getAddProduct);

// POST => /admin/add-product
router.post('/add-product',
    [
        body('title')
            .isString()
            .withMessage('Title may contain numbers and text only')
            .isLength({ min: 5 })
            .withMessage('Title must be at least 5 characters long')
            .trim(),
        body('price')
            .isFloat()
            .withMessage('Price must be a number')
            .trim(),
        body('description')
            .isLength({ min: 3, max: 400 })
            .withMessage('Description must be between 3 and 400 characters long')
            .trim()
            .not().isEmpty()
            .withMessage('Description must not be empty')
    ],
    isAuth, adminController.postAddProduct);

// get => /admin/products
router.get('/products', isAuth, adminController.getProducts);

// get => /admin/edit-product/:productId
router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

// // post => /admin/edit-product
router.post('/edit-product/',
    [
        body('title')
            .isString()
            .withMessage('Title may contain numbers and text only')
            .isLength({ min: 5 })
            .withMessage('Title must be at least 5 characters long')
            .trim(),
        body('price')
            .isFloat()
            .withMessage('Price must be a number')
            .trim(),
        body('description')
            .isLength({ min: 3, max: 400 })
            .withMessage('Description must be between 3 and 400 characters long')
            .trim()
    ],
    isAuth, adminController.postEditProduct);

// // post => /admin/edit-product
// router.post('/delete-product/', isAuth, adminController.postDeleteProduct);
router.delete('/product/:productId/', isAuth, adminController.deleteProduct);

module.exports = router;