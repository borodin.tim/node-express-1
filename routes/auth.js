const express = require('express');
const { check, body } = require('express-validator');

const authController = require('../controllers/auth');
const User = require('../models/user');

const router = express.Router();

router.get('/login', authController.getLogin);

router.get('/signup', authController.getSignup);

router.post('/login',
    [
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email')
            .normalizeEmail(),
        body('password', 'Password may contain numbers and text only and at least 3 chars long')
            .isLength({ min: 3 })
            .isAlphanumeric()
            .trim(),
    ],
    authController.postLogin);

router.post('/logout', authController.postLogout);

router.post(
    '/signup',
    [
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email')
            .custom(async (value, { req }) => {
                // if (value === 'test@test.test') {
                //     throw new Error('This email address is forbidden');
                // }
                // return true;
                const user = await User.findOne({ email: value });
                if (user) {
                    // req.flash('error', 'Email already in use');
                    // return res.redirect('/signup');
                    return Promise.reject('Email already in use');
                }
                return true;// ? Promise.resolve();
            })
            .normalizeEmail(),
        body('password', 'Password must be with numbers and text only and at least 3 chars long')
            .trim()
            .isLength({ min: 3 })
            .isAlphanumeric(),
        body('confirmPassword')
            .trim()
            .custom((value, { req }) => {
                if (value !== req.body.password) {
                    throw new Error('Passwords must match');
                }
                return true;
            }),
    ],
    authController.postSignup
);

router.get('/reset-password', authController.getResetPassword);

router.post('/reset-password', authController.postResetPassword);

router.get('/reset-password/:token', authController.getNewPassword);

router.post('/new-password', authController.postNewPassword);

module.exports = router;