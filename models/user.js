const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    resetToken: {
        type: String
    },
    resetTokenExpiration: {
        type: Date
    },
    cart: {
        items: [{
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Product',
                required: true
            },
            quantity: {
                type: Number,
                required: true
            }
        }]
    }
});

userSchema.methods.addToCart = function (product) {
    const cartProductIdx = this.cart.items.findIndex(cp => {
        return cp.product.toString() === product._id.toString()
    });
    let newQuantity = 1;
    const updatedCartItems = [... this.cart.items];

    if (cartProductIdx >= 0) {
        newQuantity = this.cart.items[cartProductIdx].quantity + 1;
        updatedCartItems[cartProductIdx].quantity = newQuantity;
    } else {
        updatedCartItems.push({
            product: product,
            quantity: newQuantity
        });
    }

    const updatedCart = {
        items: updatedCartItems
    };

    this.cart = updatedCart;
    return this.save();
}

userSchema.methods.getCart = function () {
    const productIds = this.cart.items.map(p => p.productId);
    return productIds;
}

userSchema.methods.removeFromCartById = function (productId) {
    const updatedCartItems = this.cart.items.filter(item =>
        item.product.toString() !== productId.toString()
    );

    this.cart.items = updatedCartItems;
    return this.save();
}

userSchema.methods.clearCart = function () {
    this.cart = { items: [] };
    return this.save();
}

module.exports = mongoose.model('User', userSchema);