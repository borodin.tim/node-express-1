const crypto = require('crypto');

const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const dotenv = require('dotenv');
const { validationResult } = require('express-validator');
const { throwError500 } = require('../util/errors');

dotenv.config();

const User = require('../models/user');

const ONE_HOUR = 1000 * 60 * 60;

const emailTransporter = nodemailer.createTransport({
    // host: 'smtp.mailtrap.io',
    // port: 2525,
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        // user: process.env.MAILTRAP_USERNAME,
        // pass: process.env.MAILTRAP_PASSWORD,
        type: 'OAuth2',
        user: process.env.GMAIL_ADDRESS,
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: process.env.ACCESS_TOKEN
    },
    // debug: true, // show debug output
    // logger: true // log information in console
});
// Tutorial how use gmail to send emails: https://www.youtube.com/watch?v=JJ44WA_eV8E
// My Google app name - Nodejs Shop Mailer

exports.getLogin = (req, res) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/login', {
        pageTitle: 'Login',
        path: '/login',
        isAuth: false,
        errorMessage: message,
        validationErrors: [],
        prevInput: {
            email: '',
            password: ''
        }
    });
};

exports.getSignup = (req, res) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/signup', {
        pageTitle: 'Signup',
        path: '/signup',
        isAuth: false,
        errorMessage: message,
        validationErrors: [],
        prevInput: {
            email: '',
            password: '',
            confirmPassword: ''
        }
    });
};

exports.postLogin = async (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    const errors = validationResult(req);
    const filteredErrorsArray = errors.array().filter((elem, idx, arr) =>
        idx === arr.findIndex(e => e.msg === elem.msg)
    );
    if (!errors.isEmpty()) {
        return res
            .status(422)
            .render('auth/login', {
                pageTitle: 'Login',
                path: '/login',
                isAuth: false,
                errorMessage: filteredErrorsArray,
                validationErrors: errors.array(),
                prevInput: { email, password }
            });
    }

    try {
        const user = await User.findOne({ email });
        if (!user) {
            // req.flash('error', 'Invalid credentials');
            // return res.redirect('/login');
            return res
                .status(422)
                .render('auth/login', {
                    pageTitle: 'Login',
                    path: '/login',
                    isAuth: false,
                    errorMessage: 'Invalid credentials',
                    validationErrors: [],
                    prevInput: { email, password }
                });
        }

        const passwordsMatch = await bcrypt.compare(password, user.password);
        if (passwordsMatch) {
            req.session.user = user;
            req.session.isLoggedIn = true;
            return req.session.save((err) => {
                // make sure session was created, then redirect
                return res.redirect('/');
            });
            // return res.redirect('/');
        } else {
            // req.flash('error', 'Invalid credentials');
            // res.redirect('/login');
            return res
                .status(422)
                .render('auth/login', {
                    pageTitle: 'Login',
                    path: '/login',
                    isAuth: false,
                    errorMessage: 'Invalid credentials',
                    validationErrors: [],
                    prevInput: { email, password }
                });
        }
    } catch (error) {
        return throwError500(error, next);
    }
};

exports.postLogout = (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            console.log(err);
        }
        res.redirect('/');
    });
};

exports.postSignup = async (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res
            .status(422)
            .render('auth/signup', {
                pageTitle: 'Signup',
                path: '/signup',
                isAuth: false,
                validationErrors: errors.array(),
                prevInput: {
                    email,
                    password,
                    confirmPassword: req.body.confirmPassword
                }
            });
    }

    const hashedPassword = await bcrypt.hash(password, 12);
    if (hashedPassword) {
        const newUser = new User({
            email,
            password: hashedPassword,
            cart: { items: [] }
        });
        try {
            await newUser.save();
        } catch (error) {
            return throwError500(error, next);
        }
    }

    const mailOptions = {
        // from: process.env.MAILTRAP_EMAIL,
        from: process.env.GMAIL_ADDRESS,
        to: email,
        subject: 'Signup succeeded for Nodejs Shop',
        html: '<h1>You successfully signed up!</h1>'
    };

    emailTransporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            throw new Error(error);
        }
    });

    res.redirect('/login');
};

exports.getResetPassword = (req, res) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/reset-password', {
        pageTitle: 'Reset Password',
        path: '/reset-password',
        errorMessage: message
    });
};

exports.postResetPassword = async (req, res, next) => {
    const buffer = await crypto.randomBytes(32);
    if (!buffer) {
        console.error('Buffer not created');
        return res.redirect('/reset-password');
    }
    const token = buffer.toString('hex');

    try {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            req.flash('error', 'No account with that email found');
            return res.redirect('/reset-password');
        }

        user.resetToken = token;
        user.resetTokenExpiration = Date.now() + ONE_HOUR;
        await user.save();
    } catch (error) {
        return throwError500(error, next);
    }
    const mailOptions = {
        from: process.env.GMAIL_ADDRESS,
        to: req.body.email,
        subject: 'Nodejs Shop user password reset',
        html: `
                <p>You requested a password reset</p>
                <p>Click this <a href="http://localhost:3000/reset-password/${token}">link</a> to set a new password.</p>
                <p>This link will be available for 1 hour.</p>
            `
    };

    emailTransporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
        }
    });
    res.redirect('/');
};

exports.getNewPassword = async (req, res, next) => {
    const token = req.params.token;

    try {
        const user = await User.findOne({
            resetToken: token, resetTokenExpiration:
                { $gt: Date.now() }
        });
        if (!user) {
            req.flash('error', 'No user found, or link expired');
            return res.redirect('/reset-password');
        }
    } catch (error) {
        return throwError500(error, next);
    }

    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    } else {
        message = null;
    }
    res.render('auth/new-password', {
        pageTitle: 'Change Password',
        path: '/reset-password',
        isAuth: false,
        errorMessage: message,
        userId: user._id.toString(),
        resetPasswordToken: token
    });
};

exports.postNewPassword = async (req, res, next) => {
    const newPassword = req.body.password;
    const newPasswordConfirm = req.body.confirmPassword;
    const userId = req.body.userId;
    const resetPasswordToken = req.body.resetPasswordToken;

    if (newPassword !== newPasswordConfirm) {
        req.flash('error', 'Passwords must match');
        const url = req.get('referer');
        return res.redirect(url);
    }

    try {
        const user = await User.findOne({
            resetToken: resetPasswordToken,
            resetPasswordToken: { $gt: Date.now() },
            _ud: userId
        });
        if (!user) {
            req.flash('error', 'Incorrect token');
            return res.redirect('/');
        }
        const hashedPassword = await bcrypt.hash(newPassword, 12);
        user.password = hashedPassword;
        user.resetToken = undefined;
        user.resetTokenExpiration = undefined;
        await user.save();
    } catch (error) {
        return throwError500(error, next);
    }
    // TODO: send mail that password was changed
    res.redirect('/');
}