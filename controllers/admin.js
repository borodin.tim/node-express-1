const Product = require("../models/product");
const { validationResult } = require('express-validator');
const { throwError500 } = require('../util/errors');
const { deleteFile } = require('../util/file');

const ITEMS_PER_PAGE = 2;

exports.getAddProduct = (req, res) => {
    if (!req.session.isLoggedIn) {
        return res.redirect('/login');
    }

    res.render('admin/edit-product', {
        pageTitle: "Add Product",
        path: '/admin/add-product',
        editing: false,
        validationErrors: [],
        prevInput: { title: '', imageUrl: '', description: '', price: '' }
    });
};

exports.postAddProduct = async (req, res, next) => {
    const title = req.body.title;
    const image = req.file;
    const description = req.body.description;
    const price = req.body.price;

    if (!image) {
        return res
            .status(422)
            .render('admin/edit-product', {
                pageTitle: "Add Product",
                path: '/admin/add-product',
                editing: false,
                validationErrors: [{ msg: 'Attached file is not an image' }],
                prevInput: { title, description, price }
            });
    }

    const errors = validationResult(req);
    // const filteredErrorsArray = errors.array().filter((elem, idx, arr) =>
    //     idx === arr.findIndex(e => e.msg === elem.msg)
    // );

    if (!errors.isEmpty()) {
        return res
            .status(422)
            .render('admin/edit-product', {
                pageTitle: "Add Product",
                path: '/admin/add-product',
                editing: false,
                validationErrors: errors.array(),
                prevInput: { title, description, price }
            });
    }

    const imageUrl = image.path;

    const product = new Product({
        title,
        price,
        description,
        imageUrl: imageUrl,
        userId: req.user, // saves ID only
        validationErrors: []
    });

    try {
        await product.save();
    } catch (error) {
        // return res
        //     .status(500)
        //     .render('admin/edit-product', {
        //         pageTitle: "Add Product",
        //         path: '/admin/add-product',
        //         editing: false,
        //         validationErrors: [{ msg: 'Database opetaion failed' }],
        //         prevInput: { title, imageUrl, description, price }
        //     });

        // return res
        //     .status(500)
        //     .render('500-internal-server-error', {
        //         pageTitle: "Add Product",
        //         path: '/admin/add-product',
        //         editing: false,
        //         validationErrors: [{ msg: 'Database opetaion failed' }],
        //         prevInput: { title, imageUrl, description, price }
        //     });

        return throwError500(error, next);
    }
    res.redirect('/admin/products');
};

exports.getEditProduct = async (req, res, next) => {
    const editMode = req.query.edit;
    if (!editMode) {
        return res.redirect('/');
    }

    const productId = req.params.productId;
    try {
        const product = await Product.findById(productId);
        if (product) {
            res.render('admin/edit-product', {
                pageTitle: "Edit Product",
                path: '/admin/edit-product',
                editing: editMode,
                product: product,
                validationErrors: []
            });
        } else {
            res.redirect('/');
        }
    } catch (error) {
        return throwError500(error, next);
    }
};

exports.postEditProduct = async (req, res, next) => {
    const {
        productId,
        title: updatedTitle,
        price: updatedPrice,
        description: updatedDescription
    } = req.body;
    const image = req.file;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res
            .status(422)
            .render('admin/edit-product', {
                pageTitle: "Edit Product",
                path: '/admin/edit-product',
                editing: true,
                product: {
                    _id: productId,
                    title: updatedTitle,
                    description: updatedDescription,
                    price: updatedPrice
                },
                validationErrors: errors.array()
            });
    }

    try {
        const product = await Product.findById(productId);
        if (product.userId.toString() !== req.user._id.toString()) {
            return res.redirect('/');
        }

        product.title = updatedTitle;
        product.price = updatedPrice;
        product.description = updatedDescription;
        if (image) {
            deleteFile(product.imageUrl);
            product.imageUrl = image.path;
        }
        await product.save();
    } catch (error) {
        return throwError500(error, next);
    }

    res.redirect('/admin/products');
};

exports.getProducts = async (req, res, next) => {
    const pageNumber = +req.query.page || 1;
    let totalProdsAmount;

    try {
        totalProdsAmount = await Product.find({ userId: req.user._id }).countDocuments();
        const products = await Product
            .find({ userId: req.user._id })
            .skip((pageNumber - 1) * ITEMS_PER_PAGE)
            .limit(ITEMS_PER_PAGE);;
        // .select('title price -_id') // fetch only title and price
        // .populate('userId', 'name'); // populate userId and fetch only name
        res.render('admin/products', {
            prods: products,
            currentPage: pageNumber,
            hasNextPage: ITEMS_PER_PAGE * pageNumber < totalProdsAmount,
            hasPreviousPage: pageNumber > 1,
            nextPage: pageNumber + 1,
            previousPage: pageNumber - 1,
            lastPage: Math.ceil(totalProdsAmount / ITEMS_PER_PAGE),
            pageTitle: 'Admin Products',
            path: '/admin/products'
        });
    } catch (error) {
        return throwError500(error, next);
    };
};

exports.deleteProduct = async (req, res, next) => {
    const productId = req.params.productId;
    Product.findById(productId)
        .then(product => {
            if (!product) {
                return next(new Error('Product not found'));
            }
            deleteFile(product.imageUrl);
            return Product.deleteOne({ _id: productId, userId: req.user._id });
        })
        .then(() => {
            // res.redirect('/admin/products');
            res.status(200).json({ message: 'Success' });
        })
        .catch((error) => res.status(500).json({ message: 'Deleting product failed!' }) /*throwError500(error, next)*/)
};