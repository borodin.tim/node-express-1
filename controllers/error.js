exports.getPageNotFound = (_, res) => {
    res.status(404).render('page-not-found', {
        pageTitle: 'Page Not Found!',
        path: '/404'
    });
};

exports.get500InternalServerError = (_, res) => {
    res.status(500).render('500-internal-server-error', {
        pageTitle: 'Internal Server Error!',
        path: '/500'
    });
};