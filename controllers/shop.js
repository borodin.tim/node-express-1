const fs = require('fs');
const path = require('path');

const PDFDocument = require('pdfkit');

const Product = require("../models/product");
const Order = require("../models/order");
const { throwError500 } = require('../util/errors');
const dotenv = require('dotenv');
dotenv.config();
const stripe = require('stripe')(process.env.STRIPE_PRIVATE_KEY);

const ITEMS_PER_PAGE = 2;

exports.getProducts = async (req, res, next) => {
    const pageNumber = +req.query.page || 1;
    let totalProdsAmount;

    try {
        totalProdsAmount = await Product.find().countDocuments();
        const products = await Product
            .find()
            .skip((pageNumber - 1) * ITEMS_PER_PAGE)
            .limit(ITEMS_PER_PAGE);
        res.render('shop/product-list', {
            prods: products,
            currentPage: pageNumber,
            hasNextPage: ITEMS_PER_PAGE * pageNumber < totalProdsAmount,
            hasPreviousPage: pageNumber > 1,
            nextPage: pageNumber + 1,
            previousPage: pageNumber - 1,
            lastPage: Math.ceil(totalProdsAmount / ITEMS_PER_PAGE),
            pageTitle: 'Products',
            path: '/products'
        });
    } catch (error) {
        return throwError500(error, next);
    };
};

exports.getProduct = async (req, res, next) => {
    const productId = req.params.productId;
    try {
        const product = await Product.findById(productId);
        res.render('shop/product-detail', {
            pageTitle: `Product Details: ${product.title}`,
            path: `/products`,
            product: product
        });
    } catch (error) {
        return throwError500(error, next);
    }
};

exports.getIndex = async (req, res, next) => {
    const pageNumber = +req.query.page || 1;
    let totalProdsAmount;

    try {
        totalProdsAmount = await Product.find().countDocuments();
        const products = await Product
            .find()
            .skip((pageNumber - 1) * ITEMS_PER_PAGE)
            .limit(ITEMS_PER_PAGE);
        res.render('shop/index', {
            prods: products,
            currentPage: pageNumber,
            hasNextPage: ITEMS_PER_PAGE * pageNumber < totalProdsAmount,
            hasPreviousPage: pageNumber > 1,
            nextPage: pageNumber + 1,
            previousPage: pageNumber - 1,
            lastPage: Math.ceil(totalProdsAmount / ITEMS_PER_PAGE),
            pageTitle: 'Shop',
            path: '/'
        });
    } catch (error) {
        return throwError500(error, next);
    }
};

exports.getCart = async (req, res, next) => {
    try {
        const user = await req.user.populate('cart.items.product');
        const products = user.cart.items;
        return res.render('shop/cart', {
            pageTitle: 'Your Cart',
            path: '/cart',
            prods: products
        });
    } catch (error) {
        return throwError500(error, next);
    }
};

exports.postCart = async (req, res, next) => {
    const productId = req.body.productId;
    try {
        const product = await Product.findById(productId);
        await req.user.addToCart(product);
        return res.redirect('/cart',);
    } catch (error) {
        return throwError500(error, next);
    }
};

exports.postCartRemove = async (req, res, next) => {
    const productId = req.body.productId;
    try {
        await req.user.removeFromCartById(productId);
        return res.redirect('/cart');
    } catch (error) {
        return throwError500(error, next);
    }
}

exports.getOrders = async (req, res, next) => {
    try {
        const orders = await Order.find({ "user.userId": req.user._id });
        res.render('shop/orders', {
            pageTitle: 'Your Orders',
            path: '/orders',
            orders: orders
        })
    } catch (error) {
        return throwError500(error, next);
    }
};

// exports.postOrder = async (req, res, next) => {
//     try {
//         const user = await req.user.populate('cart.items.product');
//         const order = new Order({
//             items: user.cart.items.map(item => {
//                 return {
//                     product: { ...item.product._doc },
//                     quantity: item.quantity
//                 }
//             }),
//             user: {
//                 email: req.user.email,
//                 userId: req.user
//             }
//         });
//         await order.save();
//         await req.user.clearCart();
//         res.redirect('/orders');
//     } catch (error) {
//         return throwError500(error, next);
//     }
// };


exports.getInvoice = async (req, res, next) => {
    const orderId = req.params.orderId;

    const order = await Order.findById(orderId);
    if (!order) {
        return next(new Error('No order found!'))
    }

    if (order.user.userId.toString() !== req.user._id.toString()) {
        return next(new Error('Unauthorized'));
    }

    const invoiceName = 'invoice-' + orderId + '.pdf';
    const invoicePath = path.join('data', 'invoices', invoiceName);

    const pdfDoc = new PDFDocument();
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'inline; filename="' + invoiceName + '"');
    pdfDoc.pipe(fs.createWriteStream(invoicePath));
    pdfDoc.pipe(res);

    pdfDoc.fontSize(26).text(`Invoice - ${orderId}`, {
        underline: true
    });
    pdfDoc.text(' ');

    let totalPrice = 0;
    order.items.forEach(item => {
        pdfDoc.fontSize(14).text(`Name: ${item.product.title}
Price: ${item.product.price}$
${item.product.description}
Qty: ${item.quantity}
        
        `);
        totalPrice += item.product.price * item.quantity;
    });

    pdfDoc.fontSize(20).text(`Total: ${totalPrice}$`);

    pdfDoc.end();

    // fs.readFile(invoicePath, (err, data) => {
    //     if (err) {
    //         return next(err);
    //     }
    //     res.setHeader('Content-Type', 'application/pdf');
    //     res.setHeader('Content-Disposition', 'inline; filename="' + invoiceName + '"');
    //     res.end(data);
    // });
    // const file = fs.createReadStream(invoicePath);
    // file.pipe(res);
};

exports.getCheckout = async (req, res) => {
    let total = 0;

    const user = await req.user.populate('cart.items.product');
    const products = user.cart.items;
    products.forEach(p => total += p.quantity * p.product.price);

    const session = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: products.map(p => {
            return {
                name: p.product.title,
                description: p.product.description,
                amount: p.product.price * 100,
                currency: 'usd',
                quantity: p.quantity
            };
        }),
        success_url: req.protocol + '://' + req.get('host') + '/checkout/success',
        cancel_url: req.protocol + '://' + req.get('host') + '/checkout/cancel'
    });

    return res.render('shop/checkout', {
        pageTitle: 'Checkout',
        path: '/checkout',
        prods: products,
        totalSum: total,
        sessionId: session.id
    })
};

exports.getCheckoutSuccess = async (req, res, next) => {
    try {
        const user = await req.user.populate('cart.items.product');
        const order = new Order({
            items: user.cart.items.map(item => {
                return {
                    product: { ...item.product._doc },
                    quantity: item.quantity
                }
            }),
            user: {
                email: req.user.email,
                userId: req.user
            }
        });
        await order.save();
        await req.user.clearCart();
        res.redirect('/orders');
    } catch (error) {
        return throwError500(error, next);
    }
};