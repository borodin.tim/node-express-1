const path = require('path');

// returns the path of app.js
module.exports = path.dirname(require.main.filename);