exports.throwError500 = (error, next) => {
    const err = new Error(error);
    err.httpStatusCode = 500;
    return next(err);
}