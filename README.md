# Shop application
Backend built with NodeJS, frontend - ejs.

<br/>

## Branches: 
- mongoose - using Object Data Modeling (ODM) library for MongoDB and Node.js
- sql_branch - useing SQL db
- mongodb - using plain MongoDB 

---
<br/>

### SQL 

Docs - https://sequelize.org/docs/v6/category/core-concepts/

How to install SQL on Ubuntu: https://dev.mysql.com/doc/mysql-apt-repo-quick-guide/en/

Commands:

systemctl status mysql

sudo systemctl stop mysql

---
<br/>

### Mongoose

Using Object Data Modeling (ODM) library for MongoDB and Node.js 

MongoDB Compass - app

MongoDB Atlas - WebUI


### TODO:
- Add webhooks after the payment. (https://stripe.com/docs/payments/handling-payment-events)